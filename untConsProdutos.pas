unit untConsProdutos;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  untDefault, FMX.Controls.Presentation, FMX.Objects, FMX.Layouts,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
  FMX.ListView, FMX.Edit, System.Rtti, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.Components,
  Data.Bind.DBScope, FMX.ListBox, FMX.EditBox, FMX.SpinBox;

type
  TfrmConsProdutos = class(TfrmDefault)
    lblConsProdutos: TLabel;
    LytMain: TLayout;
    lytTop: TLayout;
    lytLeft: TLayout;
    lytRight: TLayout;
    ListView1: TListView;
    Label2: TLabel;
    Line1: TLine;
    edtProdA: TEdit;
    edtProdB: TEdit;
    edtProdC: TEdit;
    edtProdD: TEdit;
    btnBuscar: TButton;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    cmbGrupo: TComboBox;
    lblGrupo: TLabel;
    SpinBox1: TSpinBox;
    SpinBox2: TSpinBox;
    CheckBox1: TCheckBox;
    procedure btnBuscarClick(Sender: TObject);
    procedure edtProdAKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure edtProdBKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure edtProdCKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure edtProdDKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsProdutos: TfrmConsProdutos;

implementation

{$R *.fmx}

uses untDM;

procedure TfrmConsProdutos.btnBuscarClick(Sender: TObject);
begin
  inherited;
    DM.qryProdutos.Close;
    DM.qryProdutos.SQL.Clear;

    DM.qryProdutos.SQL.Add('SELECT * FROM PRODUTOS WHERE ID > 0 ');
    if TRIM(edtProdA.Text) <> '' then
      DM.qryProdutos.SQL.Add('and substr(codigo_sem_pontos,1,2) like ''' +Trim(edtProdA.Text)+ '%'' ');
    if TRIM(edtProdB.Text) <> '' then
      DM.qryProdutos.SQL.Add('and substr(codigo_sem_pontos,3,4) like ''' +Trim(edtProdB.Text)+ '%'' ');
    if TRIM(edtProdC.Text) <> '' then
      DM.qryProdutos.SQL.Add('and substr(codigo_sem_pontos,7,1) like ''' +Trim(edtProdC.Text)+ '%'' ');
    if TRIM(edtProdD.Text) <> '' then
      DM.qryProdutos.SQL.Add('and substr(codigo_sem_pontos,8,3) like ''' +Trim(edtProdD.Text)+ '%'' ');
    if cmbGrupo.items[cmbGrupo.ItemIndex] <> 'NENHUM' then
      DM.qryProdutos.SQL.Add('and GRUPO LIKE ''' +cmbGrupo.items[cmbGrupo.ItemIndex]+ ''' ');



    DM.qryProdutos.Open;
   // DM.qryProdutos.Open('SELECT * FROM PRODUTOS WHERE GRUPO LIKE ''' +combobox1.items[combobox1.ItemIndex]+ '''');
end;

procedure TfrmConsProdutos.edtProdAKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  inherited;
{  if edtProdA.Text.Length = 2 then
  begin
    ShowMessage('Apenas dois caracteres s�o permitidos.');
    //edtProdA.Text := Copy(edtProdA.Text,1,edtProdA.Text.Length-1);
    Abort


  end;}

  if key=vkReturn then
    if edtProdB.CanFocus then edtProdB.SetFocus;
end;

procedure TfrmConsProdutos.edtProdBKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  inherited;
  if key=vkReturn then
    if edtProdC.CanFocus then edtProdC.SetFocus;
  if key=vkBack then
    if edtProdA.CanFocus then edtProdA.SetFocus;
end;

procedure TfrmConsProdutos.edtProdCKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  inherited;
  if key=vkReturn then
    if edtProdD.CanFocus then edtProdD.SetFocus;
  if key=vkBack then
    if edtProdB.CanFocus then edtProdB.SetFocus;
end;

procedure TfrmConsProdutos.edtProdDKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  inherited;
  if key=vkReturn then
    if btnBuscar.CanFocus then btnBuscar.SetFocus;
  if key=vkBack then
    if edtProdC.CanFocus then edtProdC.SetFocus;
end;

procedure TfrmConsProdutos.FormShow(Sender: TObject);
begin
  inherited;
  DM.qryAux.Close;
  DM.qryAux.SQL.Clear;
  DM.qryAux.Open('select DISTINCT(PRODUTOS.GRUPO) from PRODUTOS');
  cmbGrupo.Items.Add('NENHUM');
  while not DM.qryAux.Eof do
  begin
    cmbGrupo.Items.Add(DM.qryAux.FieldByName('grupo').AsString);
    DM.qryAux.Next;
  end;

  cmbGrupo.ItemIndex := 0;
end;

end.

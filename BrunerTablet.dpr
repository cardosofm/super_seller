program BrunerTablet;



{$R *.dres}

uses
  System.StartUpCopy,
  FMX.Forms,
  untMain in 'untMain.pas' {frmMain},
  untDefault in 'untDefault.pas' {frmDefault},
  untConfiguracoes in 'untConfiguracoes.pas' {frmConfiguracoes},
  untLib in 'untLib.pas',
  untDM in 'untDM.pas' {DM: TDataModule},
  untTestes in 'untTestes.pas' {frmTestes},
  ClientModuleUnit1 in 'ClientModuleUnit1.pas' {ClientModule1: TDataModule},
  ClientClassesUnit1 in 'ClientClassesUnit1.pas',
  untConsProdutos in 'untConsProdutos.pas' {frmConsProdutos};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TClientModule1, ClientModule1);
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmConfiguracoes, frmConfiguracoes);
  Application.CreateForm(TfrmTestes, frmTestes);
  Application.CreateForm(TfrmConsProdutos, frmConsProdutos);
  Application.Run;
end.

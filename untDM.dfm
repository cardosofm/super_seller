object DM: TDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 414
  Width = 539
  object conSQLite: TFDConnection
    Params.Strings = (
      'DriverID=SQLite')
    UpdateOptions.AssignedValues = [uvAutoCommitUpdates]
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 40
    Top = 24
  end
  object qrySystem: TFDQuery
    CachedUpdates = True
    Connection = conSQLite
    SQL.Strings = (
      'select * from system')
    Left = 96
    Top = 24
    object qrySystemID: TStringField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Size = 6
    end
    object qrySystemSENHA: TStringField
      FieldName = 'SENHA'
      Origin = 'SENHA'
    end
    object qrySystemDTH_ULTIMA_ATUALIZACAO: TSQLTimeStampField
      FieldName = 'DTH_ULTIMA_ATUALIZACAO'
      Origin = 'DTH_ULTIMA_ATUALIZACAO'
    end
    object qrySystemIMAGEM: TBlobField
      FieldName = 'IMAGEM'
      Origin = 'IMAGEM'
    end
    object qrySystemNOME: TStringField
      FieldName = 'NOME'
      Origin = 'NOME'
      Size = 150
    end
    object qrySystemVERSAO: TStringField
      FieldName = 'VERSAO'
      Origin = 'VERSAO'
      Size = 32767
    end
    object qrySystemADMIN: TBooleanField
      FieldName = 'ADMIN'
      Origin = 'ADMIN'
    end
    object qrySystemAREAS: TStringField
      FieldName = 'AREAS'
      Origin = 'AREAS'
      Size = 100
    end
  end
  object mt: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 200
    Top = 32
  end
  object qryAux: TFDQuery
    Connection = conSQLite
    Left = 288
    Top = 32
  end
  object qryProdutos: TFDQuery
    CachedUpdates = True
    Connection = conSQLite
    SQL.Strings = (
      'select * from produtos')
    Left = 88
    Top = 160
    object qryProdutosID: TStringField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object qryProdutosCODIGO: TStringField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
    end
    object qryProdutosCODIGO_SEM_PONTOS: TStringField
      FieldName = 'CODIGO_SEM_PONTOS'
      Origin = 'CODIGO_SEM_PONTOS'
    end
    object qryProdutosVLR_AU_ICMS_18: TBCDField
      FieldName = 'VLR_AU_ICMS_18'
      Origin = 'VLR_AU_ICMS_18'
      Precision = 18
      Size = 3
    end
    object qryProdutosVLR_AU_ICMS_12: TBCDField
      FieldName = 'VLR_AU_ICMS_12'
      Origin = 'VLR_AU_ICMS_12'
      Precision = 18
      Size = 3
    end
    object qryProdutosVLR_AU_ICMS_7: TBCDField
      FieldName = 'VLR_AU_ICMS_7'
      Origin = 'VLR_AU_ICMS_7'
      Precision = 18
      Size = 3
    end
    object qryProdutosVLR_AU_EXP: TBCDField
      FieldName = 'VLR_AU_EXP'
      Origin = 'VLR_AU_EXP'
      Precision = 18
      Size = 3
    end
    object qryProdutosVLR_AU_ICMS_MANAUS: TBCDField
      FieldName = 'VLR_AU_ICMS_MANAUS'
      Origin = 'VLR_AU_ICMS_MANAUS'
      Precision = 18
      Size = 3
    end
    object qryProdutosCOTACAO_AU: TBCDField
      FieldName = 'COTACAO_AU'
      Origin = 'COTACAO_AU'
      Precision = 18
      Size = 3
    end
    object qryProdutosCOTACAO_US: TBCDField
      FieldName = 'COTACAO_US'
      Origin = 'COTACAO_US'
      Precision = 18
      Size = 3
    end
    object qryProdutosDESCRICAO_PRODUTO: TStringField
      FieldName = 'DESCRICAO_PRODUTO'
      Origin = 'DESCRICAO_PRODUTO'
      Size = 200
    end
    object qryProdutosATIVO: TStringField
      FieldName = 'ATIVO'
      Origin = 'ATIVO'
      Size = 1
    end
    object qryProdutosGRUPO: TStringField
      FieldName = 'GRUPO'
      Origin = 'GRUPO'
      Size = 100
    end
    object qryProdutosSUBGRUPO: TStringField
      FieldName = 'SUBGRUPO'
      Origin = 'SUBGRUPO'
      Size = 100
    end
    object qryProdutosTEOR: TIntegerField
      FieldName = 'TEOR'
      Origin = 'TEOR'
    end
    object qryProdutosUNIDADE: TStringField
      FieldName = 'UNIDADE'
      Origin = 'UNIDADE'
      Size = 50
    end
    object qryProdutosDESCRICAO_COLECAO: TStringField
      FieldName = 'DESCRICAO_COLECAO'
      Origin = 'DESCRICAO_COLECAO'
      Size = 100
    end
    object qryProdutosDESCRICAO_LINHA: TStringField
      FieldName = 'DESCRICAO_LINHA'
      Origin = 'DESCRICAO_LINHA'
      Size = 100
    end
    object qryProdutosCONTROLADO_ARO: TStringField
      FieldName = 'CONTROLADO_ARO'
      Origin = 'CONTROLADO_ARO'
      Size = 1
    end
    object qryProdutosDTH_INATIVO: TSQLTimeStampField
      FieldName = 'DTH_INATIVO'
      Origin = 'DTH_INATIVO'
    end
    object qryProdutosDTH_CRIACAO: TSQLTimeStampField
      FieldName = 'DTH_CRIACAO'
      Origin = 'DTH_CRIACAO'
    end
    object qryProdutosPESO_MEDIO: TBCDField
      FieldName = 'PESO_MEDIO'
      Origin = 'PESO_MEDIO'
      Precision = 18
      Size = 3
    end
    object qryProdutosQTD_BRILHANTES: TBCDField
      FieldName = 'QTD_BRILHANTES'
      Origin = 'QTD_BRILHANTES'
      Precision = 18
      Size = 3
    end
    object qryProdutosQUIL_BRILHANTES: TBCDField
      FieldName = 'QUIL_BRILHANTES'
      Origin = 'QUIL_BRILHANTES'
      Precision = 18
      Size = 3
    end
    object qryProdutosQTD_PEDRAS: TBCDField
      FieldName = 'QTD_PEDRAS'
      Origin = 'QTD_PEDRAS'
      Precision = 18
      Size = 3
    end
    object qryProdutosQUIL_PEDRAS: TBCDField
      FieldName = 'QUIL_PEDRAS'
      Origin = 'QUIL_PEDRAS'
      Precision = 18
      Size = 3
    end
    object qryProdutosALTURA: TBCDField
      FieldName = 'ALTURA'
      Origin = 'ALTURA'
      Precision = 18
      Size = 3
    end
    object qryProdutosLARGURA: TBCDField
      FieldName = 'LARGURA'
      Origin = 'LARGURA'
      Precision = 18
      Size = 3
    end
    object qryProdutosDIAMETRO: TBCDField
      FieldName = 'DIAMETRO'
      Origin = 'DIAMETRO'
      Precision = 18
      Size = 3
    end
    object qryProdutosPERFIL_INTERNO: TStringField
      FieldName = 'PERFIL_INTERNO'
      Origin = 'PERFIL_INTERNO'
    end
    object qryProdutosPERFIL_EXTERNO: TStringField
      FieldName = 'PERFIL_EXTERNO'
      Origin = 'PERFIL_EXTERNO'
    end
    object qryProdutosDESCRICAO_PEDRAS: TStringField
      FieldName = 'DESCRICAO_PEDRAS'
      Origin = 'DESCRICAO_PEDRAS'
      Size = 150
    end
    object qryProdutosARO: TIntegerField
      FieldName = 'ARO'
      Origin = 'ARO'
    end
  end
  object FDUpdateSQL1: TFDUpdateSQL
    Connection = conSQLite
    InsertSQL.Strings = (
      'INSERT INTO PRODUTOS'
      '(ID, CODIGO, CODIGO_SEM_PONTOS, VLR_AU_ICMS_18, '
      '  VLR_AU_ICMS_12, VLR_AU_ICMS_7, VLR_AU_EXP, '
      '  VLR_AU_ICMS_MANAUS, COTACAO_AU, COTACAO_US, '
      '  DESCRICAO_PRODUTO, ATIVO, GRUPO, SUBGRUPO, '
      '  TEOR, UNIDADE, DESCRICAO_COLECAO, DESCRICAO_LINHA, '
      '  CONTROLADO_ARO, DTH_INATIVO, DTH_CRIACAO, '
      '  PESO_MEDIO, QTD_BRILHANTES, QUIL_BRILHANTES, '
      '  QTD_PEDRAS, QUIL_PEDRAS, ALTURA, LARGURA, '
      '  DIAMETRO, PERFIL_INTERNO, PERFIL_EXTERNO, '
      '  DESCRICAO_PEDRAS, ARO)'
      
        'VALUES (:NEW_ID, :NEW_CODIGO, :NEW_CODIGO_SEM_PONTOS, :NEW_VLR_A' +
        'U_ICMS_18, '
      '  :NEW_VLR_AU_ICMS_12, :NEW_VLR_AU_ICMS_7, :NEW_VLR_AU_EXP, '
      '  :NEW_VLR_AU_ICMS_MANAUS, :NEW_COTACAO_AU, :NEW_COTACAO_US, '
      
        '  :NEW_DESCRICAO_PRODUTO, :NEW_ATIVO, :NEW_GRUPO, :NEW_SUBGRUPO,' +
        ' '
      
        '  :NEW_TEOR, :NEW_UNIDADE, :NEW_DESCRICAO_COLECAO, :NEW_DESCRICA' +
        'O_LINHA, '
      '  :NEW_CONTROLADO_ARO, :NEW_DTH_INATIVO, :NEW_DTH_CRIACAO, '
      '  :NEW_PESO_MEDIO, :NEW_QTD_BRILHANTES, :NEW_QUIL_BRILHANTES, '
      '  :NEW_QTD_PEDRAS, :NEW_QUIL_PEDRAS, :NEW_ALTURA, :NEW_LARGURA, '
      '  :NEW_DIAMETRO, :NEW_PERFIL_INTERNO, :NEW_PERFIL_EXTERNO, '
      '  :NEW_DESCRICAO_PEDRAS, :NEW_ARO)')
    ModifySQL.Strings = (
      'UPDATE PRODUTOS'
      
        'SET ID = :NEW_ID, CODIGO = :NEW_CODIGO, CODIGO_SEM_PONTOS = :NEW' +
        '_CODIGO_SEM_PONTOS, '
      
        '  VLR_AU_ICMS_18 = :NEW_VLR_AU_ICMS_18, VLR_AU_ICMS_12 = :NEW_VL' +
        'R_AU_ICMS_12, '
      
        '  VLR_AU_ICMS_7 = :NEW_VLR_AU_ICMS_7, VLR_AU_EXP = :NEW_VLR_AU_E' +
        'XP, '
      
        '  VLR_AU_ICMS_MANAUS = :NEW_VLR_AU_ICMS_MANAUS, COTACAO_AU = :NE' +
        'W_COTACAO_AU, '
      
        '  COTACAO_US = :NEW_COTACAO_US, DESCRICAO_PRODUTO = :NEW_DESCRIC' +
        'AO_PRODUTO, '
      
        '  ATIVO = :NEW_ATIVO, GRUPO = :NEW_GRUPO, SUBGRUPO = :NEW_SUBGRU' +
        'PO, '
      
        '  TEOR = :NEW_TEOR, UNIDADE = :NEW_UNIDADE, DESCRICAO_COLECAO = ' +
        ':NEW_DESCRICAO_COLECAO, '
      
        '  DESCRICAO_LINHA = :NEW_DESCRICAO_LINHA, CONTROLADO_ARO = :NEW_' +
        'CONTROLADO_ARO, '
      
        '  DTH_INATIVO = :NEW_DTH_INATIVO, DTH_CRIACAO = :NEW_DTH_CRIACAO' +
        ', '
      
        '  PESO_MEDIO = :NEW_PESO_MEDIO, QTD_BRILHANTES = :NEW_QTD_BRILHA' +
        'NTES, '
      
        '  QUIL_BRILHANTES = :NEW_QUIL_BRILHANTES, QTD_PEDRAS = :NEW_QTD_' +
        'PEDRAS, '
      
        '  QUIL_PEDRAS = :NEW_QUIL_PEDRAS, ALTURA = :NEW_ALTURA, LARGURA ' +
        '= :NEW_LARGURA, '
      
        '  DIAMETRO = :NEW_DIAMETRO, PERFIL_INTERNO = :NEW_PERFIL_INTERNO' +
        ', '
      
        '  PERFIL_EXTERNO = :NEW_PERFIL_EXTERNO, DESCRICAO_PEDRAS = :NEW_' +
        'DESCRICAO_PEDRAS, '
      '  ARO = :NEW_ARO'
      
        'WHERE ID = :OLD_ID AND CODIGO = :OLD_CODIGO AND CODIGO_SEM_PONTO' +
        'S = :OLD_CODIGO_SEM_PONTOS AND '
      
        '  VLR_AU_ICMS_18 = :OLD_VLR_AU_ICMS_18 AND VLR_AU_ICMS_12 = :OLD' +
        '_VLR_AU_ICMS_12 AND '
      
        '  VLR_AU_ICMS_7 = :OLD_VLR_AU_ICMS_7 AND VLR_AU_EXP = :OLD_VLR_A' +
        'U_EXP AND '
      
        '  VLR_AU_ICMS_MANAUS = :OLD_VLR_AU_ICMS_MANAUS AND COTACAO_AU = ' +
        ':OLD_COTACAO_AU AND '
      
        '  COTACAO_US = :OLD_COTACAO_US AND DESCRICAO_PRODUTO = :OLD_DESC' +
        'RICAO_PRODUTO AND '
      
        '  ATIVO = :OLD_ATIVO AND GRUPO = :OLD_GRUPO AND SUBGRUPO = :OLD_' +
        'SUBGRUPO AND '
      
        '  TEOR = :OLD_TEOR AND UNIDADE = :OLD_UNIDADE AND DESCRICAO_COLE' +
        'CAO = :OLD_DESCRICAO_COLECAO AND '
      
        '  DESCRICAO_LINHA = :OLD_DESCRICAO_LINHA AND CONTROLADO_ARO = :O' +
        'LD_CONTROLADO_ARO AND '
      
        '  DTH_INATIVO = :OLD_DTH_INATIVO AND DTH_CRIACAO = :OLD_DTH_CRIA' +
        'CAO AND '
      
        '  PESO_MEDIO = :OLD_PESO_MEDIO AND QTD_BRILHANTES = :OLD_QTD_BRI' +
        'LHANTES AND '
      
        '  QUIL_BRILHANTES = :OLD_QUIL_BRILHANTES AND QTD_PEDRAS = :OLD_Q' +
        'TD_PEDRAS AND '
      '  QUIL_PEDRAS = :OLD_QUIL_PEDRAS AND ALTURA = :OLD_ALTURA AND '
      '  LARGURA = :OLD_LARGURA AND DIAMETRO = :OLD_DIAMETRO AND '
      
        '  PERFIL_INTERNO = :OLD_PERFIL_INTERNO AND PERFIL_EXTERNO = :OLD' +
        '_PERFIL_EXTERNO AND '
      '  DESCRICAO_PEDRAS = :OLD_DESCRICAO_PEDRAS AND ARO = :OLD_ARO')
    DeleteSQL.Strings = (
      'DELETE FROM PRODUTOS'
      
        'WHERE ID = :OLD_ID AND CODIGO = :OLD_CODIGO AND CODIGO_SEM_PONTO' +
        'S = :OLD_CODIGO_SEM_PONTOS AND '
      
        '  VLR_AU_ICMS_18 = :OLD_VLR_AU_ICMS_18 AND VLR_AU_ICMS_12 = :OLD' +
        '_VLR_AU_ICMS_12 AND '
      
        '  VLR_AU_ICMS_7 = :OLD_VLR_AU_ICMS_7 AND VLR_AU_EXP = :OLD_VLR_A' +
        'U_EXP AND '
      
        '  VLR_AU_ICMS_MANAUS = :OLD_VLR_AU_ICMS_MANAUS AND COTACAO_AU = ' +
        ':OLD_COTACAO_AU AND '
      
        '  COTACAO_US = :OLD_COTACAO_US AND DESCRICAO_PRODUTO = :OLD_DESC' +
        'RICAO_PRODUTO AND '
      
        '  ATIVO = :OLD_ATIVO AND GRUPO = :OLD_GRUPO AND SUBGRUPO = :OLD_' +
        'SUBGRUPO AND '
      
        '  TEOR = :OLD_TEOR AND UNIDADE = :OLD_UNIDADE AND DESCRICAO_COLE' +
        'CAO = :OLD_DESCRICAO_COLECAO AND '
      
        '  DESCRICAO_LINHA = :OLD_DESCRICAO_LINHA AND CONTROLADO_ARO = :O' +
        'LD_CONTROLADO_ARO AND '
      
        '  DTH_INATIVO = :OLD_DTH_INATIVO AND DTH_CRIACAO = :OLD_DTH_CRIA' +
        'CAO AND '
      
        '  PESO_MEDIO = :OLD_PESO_MEDIO AND QTD_BRILHANTES = :OLD_QTD_BRI' +
        'LHANTES AND '
      
        '  QUIL_BRILHANTES = :OLD_QUIL_BRILHANTES AND QTD_PEDRAS = :OLD_Q' +
        'TD_PEDRAS AND '
      '  QUIL_PEDRAS = :OLD_QUIL_PEDRAS AND ALTURA = :OLD_ALTURA AND '
      '  LARGURA = :OLD_LARGURA AND DIAMETRO = :OLD_DIAMETRO AND '
      
        '  PERFIL_INTERNO = :OLD_PERFIL_INTERNO AND PERFIL_EXTERNO = :OLD' +
        '_PERFIL_EXTERNO AND '
      '  DESCRICAO_PEDRAS = :OLD_DESCRICAO_PEDRAS AND ARO = :OLD_ARO')
    FetchRowSQL.Strings = (
      
        'SELECT ID, CODIGO, CODIGO_SEM_PONTOS, VLR_AU_ICMS_18, VLR_AU_ICM' +
        'S_12, '
      '  VLR_AU_ICMS_7, VLR_AU_EXP, VLR_AU_ICMS_MANAUS, COTACAO_AU, '
      '  COTACAO_US, DESCRICAO_PRODUTO, ATIVO, GRUPO, SUBGRUPO, TEOR, '
      '  UNIDADE, DESCRICAO_COLECAO, DESCRICAO_LINHA, CONTROLADO_ARO, '
      
        '  DTH_INATIVO, DTH_CRIACAO, PESO_MEDIO, QTD_BRILHANTES, QUIL_BRI' +
        'LHANTES, '
      
        '  QTD_PEDRAS, QUIL_PEDRAS, ALTURA, LARGURA, DIAMETRO, PERFIL_INT' +
        'ERNO, '
      '  PERFIL_EXTERNO, DESCRICAO_PEDRAS, ARO'
      'FROM PRODUTOS'
      
        'WHERE ID = :ID AND CODIGO = :CODIGO AND CODIGO_SEM_PONTOS = :COD' +
        'IGO_SEM_PONTOS AND '
      
        '  VLR_AU_ICMS_18 = :VLR_AU_ICMS_18 AND VLR_AU_ICMS_12 = :VLR_AU_' +
        'ICMS_12 AND '
      
        '  VLR_AU_ICMS_7 = :VLR_AU_ICMS_7 AND VLR_AU_EXP = :VLR_AU_EXP AN' +
        'D '
      
        '  VLR_AU_ICMS_MANAUS = :VLR_AU_ICMS_MANAUS AND COTACAO_AU = :COT' +
        'ACAO_AU AND '
      
        '  COTACAO_US = :COTACAO_US AND DESCRICAO_PRODUTO = :DESCRICAO_PR' +
        'ODUTO AND '
      
        '  ATIVO = :ATIVO AND GRUPO = :GRUPO AND SUBGRUPO = :SUBGRUPO AND' +
        ' '
      
        '  TEOR = :TEOR AND UNIDADE = :UNIDADE AND DESCRICAO_COLECAO = :D' +
        'ESCRICAO_COLECAO AND '
      
        '  DESCRICAO_LINHA = :DESCRICAO_LINHA AND CONTROLADO_ARO = :CONTR' +
        'OLADO_ARO AND '
      '  DTH_INATIVO = :DTH_INATIVO AND DTH_CRIACAO = :DTH_CRIACAO AND '
      
        '  PESO_MEDIO = :PESO_MEDIO AND QTD_BRILHANTES = :QTD_BRILHANTES ' +
        'AND '
      
        '  QUIL_BRILHANTES = :QUIL_BRILHANTES AND QTD_PEDRAS = :QTD_PEDRA' +
        'S AND '
      
        '  QUIL_PEDRAS = :QUIL_PEDRAS AND ALTURA = :ALTURA AND LARGURA = ' +
        ':LARGURA AND '
      '  DIAMETRO = :DIAMETRO AND PERFIL_INTERNO = :PERFIL_INTERNO AND '
      
        '  PERFIL_EXTERNO = :PERFIL_EXTERNO AND DESCRICAO_PEDRAS = :DESCR' +
        'ICAO_PEDRAS AND '
      '  ARO = :ARO')
    Left = 88
    Top = 240
  end
  object Command: TFDCommand
    Connection = conSQLite
    Left = 376
    Top = 216
  end
end

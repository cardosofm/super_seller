unit untTestes;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  untDefault, FMX.Controls.Presentation, FMX.Objects, FMX.TMSBaseControl,
  FMX.TMSGridCell, FMX.TMSGridOptions, FMX.TMSGridData, FMX.TMSCustomGrid,
  FMX.TMSGrid, System.Rtti, FMX.Grid.Style, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, FMX.ScrollBox,
  FMX.Grid, FMX.ListBox, FMX.Edit, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, FMX.ListView;

type
  TfrmTestes = class(TfrmDefault)
    Button1: TButton;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    ComboBox1: TComboBox;
    Button2: TButton;
    Button3: TButton;
    Edit1: TEdit;
    ListView1: TListView;
    LinkListControlToField1: TLinkListControlToField;
    procedure btnVoltarClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTestes: TfrmTestes;

implementation

{$R *.fmx}

uses untDM;

procedure TfrmTestes.btnVoltarClick(Sender: TObject);
begin
  inherited;
  frmTestes.Hide;
end;

procedure TfrmTestes.Button1Click(Sender: TObject);
begin
  inherited;
    DM.qryProdutos.Close;
    DM.qryProdutos.SQL.Clear;

    DM.qryProdutos.Open('SELECT * FROM PRODUTOS WHERE GRUPO LIKE ''' +combobox1.items[combobox1.ItemIndex]+ '''');
end;

procedure TfrmTestes.Button2Click(Sender: TObject);
begin
  inherited;
  DM.qryProdutos.Next;
end;

procedure TfrmTestes.Button3Click(Sender: TObject);
begin
  inherited;
  DM.qryProdutos.Prior;
end;

procedure TfrmTestes.FormShow(Sender: TObject);
begin
  inherited;
  DM.qryAux.Close;
  DM.qryAux.SQL.Clear;
  DM.qryAux.Open('select DISTINCT(PRODUTOS.GRUPO) from PRODUTOS');
  while not DM.qryAux.Eof do
  begin
    ComboBox1.Items.Add(DM.qryAux.FieldByName('grupo').AsString);
    DM.qryAux.Next;
  end;
end;

end.

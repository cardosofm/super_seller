//
// Created by the DataSnap proxy generator.
// 07/06/2019 08:08:13
//

unit ClientClassesUnit1;

interface

uses System.JSON, Datasnap.DSProxyRest, Datasnap.DSClientRest, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.FireDACJSONReflect, Data.DBXJSONReflect;

type

  IDSRestCachedTFDJSONDataSets = interface;

  TServerMethods1Client = class(TDSAdminRestClient)
  private
    FDataModuleCreateCommand: TDSRestCommand;
    FEchoStringCommand: TDSRestCommand;
    FReverseStringCommand: TDSRestCommand;
    FCalcPrecoCommand: TDSRestCommand;
    FConverterReaisCommand: TDSRestCommand;
    FChecaProdutoExisteCommand: TDSRestCommand;
    FDownloadFileCommand: TDSRestCommand;
    FDownloadFileCommand_Cache: TDSRestCommand;
    FGetDadosProdutoCommand: TDSRestCommand;
    FGetDadosProdutoCommand_Cache: TDSRestCommand;
    FGetDadosProdutoLojaCommand: TDSRestCommand;
    FGetDadosProdutoLojaCommand_Cache: TDSRestCommand;
    FGetProdutosCommand: TDSRestCommand;
    FGetProdutosCommand_Cache: TDSRestCommand;
    FGetSaldoCommand: TDSRestCommand;
    FGetSaldoCommand_Cache: TDSRestCommand;
    FValidaUsuarioSenhaCommand: TDSRestCommand;
    FGetConjuntoCommand: TDSRestCommand;
    FGetConjuntoCommand_Cache: TDSRestCommand;
    FGetCampanhasCommand: TDSRestCommand;
    FGetCampanhasCommand_Cache: TDSRestCommand;
    FGetClientesCommand: TDSRestCommand;
    FGetClientesCommand_Cache: TDSRestCommand;
    FGetRangePrecosCommand: TDSRestCommand;
    FGetRangePrecosCommand_Cache: TDSRestCommand;
    FChecaVendeporAroCommand: TDSRestCommand;
    FValidaUsuarioSenha2Command: TDSRestCommand;
    FGetDadosUsuarioCommand: TDSRestCommand;
    FGetDadosUsuarioCommand_Cache: TDSRestCommand;
    FGetTodosProdutosCommand: TDSRestCommand;
    FGetTodosProdutosCommand_Cache: TDSRestCommand;
  public
    constructor Create(ARestConnection: TDSRestConnection); overload;
    constructor Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    procedure DataModuleCreate(Sender: TObject);
    function EchoString(Value: string; const ARequestFilter: string = ''): string;
    function ReverseString(Value: string; const ARequestFilter: string = ''): string;
    function CalcPreco(codprod: string; polpreco: Integer; peso: Currency; const ARequestFilter: string = ''): Currency;
    function ConverterReais(Valor: Currency; const ARequestFilter: string = ''): Currency;
    function ChecaProdutoExiste(Codprod: string; const ARequestFilter: string = ''): Boolean;
    function DownloadFile(AFile: string; out Size: Int64; const ARequestFilter: string = ''): TStream;
    function DownloadFile_Cache(AFile: string; out Size: Int64; const ARequestFilter: string = ''): IDSRestCachedStream;
    function GetDadosProduto(CodProd: string; CodPolPreco: Integer; Aro: Integer; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetDadosProduto_Cache(CodProd: string; CodPolPreco: Integer; Aro: Integer; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function GetDadosProdutoLoja(CodProd: string; PolPreco: Integer; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetDadosProdutoLoja_Cache(CodProd: string; PolPreco: Integer; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function GetProdutos(Codprod: string; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetProdutos_Cache(Codprod: string; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function GetSaldo(CodProd: string; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetSaldo_Cache(CodProd: string; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function ValidaUsuarioSenha(usuario: string; senha: string; const ARequestFilter: string = ''): Boolean;
    function GetConjunto(PolPreco: Integer; CodProd: string; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetConjunto_Cache(PolPreco: Integer; CodProd: string; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function GetCampanhas(const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetCampanhas_Cache(const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function GetClientes(Local: string; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetClientes_Cache(Local: string; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function GetRangePrecos(const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetRangePrecos_Cache(const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function ChecaVendeporAro(Codprod: string; const ARequestFilter: string = ''): Boolean;
    function ValidaUsuarioSenha2(id: string; senha: string; const ARequestFilter: string = ''): Boolean;
    function GetDadosUsuario(id: string; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetDadosUsuario_Cache(id: string; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function GetTodosProdutos(const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetTodosProdutos_Cache(const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
  end;

  IDSRestCachedTFDJSONDataSets = interface(IDSRestCachedObject<TFDJSONDataSets>)
  end;

  TDSRestCachedTFDJSONDataSets = class(TDSRestCachedObject<TFDJSONDataSets>, IDSRestCachedTFDJSONDataSets, IDSRestCachedCommand)
  end;

const
  TServerMethods1_DataModuleCreate: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: 'Sender'; Direction: 1; DBXType: 37; TypeName: 'TObject')
  );

  TServerMethods1_EchoString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethods1_ReverseString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethods1_CalcPreco: array [0..3] of TDSRestParameterMetaData =
  (
    (Name: 'codprod'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'polpreco'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'peso'; Direction: 1; DBXType: 25; TypeName: 'Currency'),
    (Name: ''; Direction: 4; DBXType: 25; TypeName: 'Currency')
  );

  TServerMethods1_ConverterReais: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Valor'; Direction: 1; DBXType: 25; TypeName: 'Currency'),
    (Name: ''; Direction: 4; DBXType: 25; TypeName: 'Currency')
  );

  TServerMethods1_ChecaProdutoExiste: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Codprod'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 4; TypeName: 'Boolean')
  );

  TServerMethods1_DownloadFile: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'AFile'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'Size'; Direction: 2; DBXType: 18; TypeName: 'Int64'),
    (Name: ''; Direction: 4; DBXType: 33; TypeName: 'TStream')
  );

  TServerMethods1_DownloadFile_Cache: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'AFile'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'Size'; Direction: 2; DBXType: 18; TypeName: 'Int64'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_GetDadosProduto: array [0..3] of TDSRestParameterMetaData =
  (
    (Name: 'CodProd'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'CodPolPreco'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'Aro'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_GetDadosProduto_Cache: array [0..3] of TDSRestParameterMetaData =
  (
    (Name: 'CodProd'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'CodPolPreco'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'Aro'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_GetDadosProdutoLoja: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'CodProd'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'PolPreco'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_GetDadosProdutoLoja_Cache: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'CodProd'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'PolPreco'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_GetProdutos: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Codprod'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_GetProdutos_Cache: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Codprod'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_GetSaldo: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'CodProd'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_GetSaldo_Cache: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'CodProd'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_ValidaUsuarioSenha: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'usuario'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'senha'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 4; TypeName: 'Boolean')
  );

  TServerMethods1_GetConjunto: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'PolPreco'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'CodProd'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_GetConjunto_Cache: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'PolPreco'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'CodProd'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_GetCampanhas: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_GetCampanhas_Cache: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_GetClientes: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Local'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_GetClientes_Cache: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Local'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_GetRangePrecos: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_GetRangePrecos_Cache: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_ChecaVendeporAro: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Codprod'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 4; TypeName: 'Boolean')
  );

  TServerMethods1_ValidaUsuarioSenha2: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'id'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'senha'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 4; TypeName: 'Boolean')
  );

  TServerMethods1_GetDadosUsuario: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'id'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_GetDadosUsuario_Cache: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'id'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods1_GetTodosProdutos: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods1_GetTodosProdutos_Cache: array [0..0] of TDSRestParameterMetaData =
  (
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

implementation

procedure TServerMethods1Client.DataModuleCreate(Sender: TObject);
begin
  if FDataModuleCreateCommand = nil then
  begin
    FDataModuleCreateCommand := FConnection.CreateCommand;
    FDataModuleCreateCommand.RequestType := 'POST';
    FDataModuleCreateCommand.Text := 'TServerMethods1."DataModuleCreate"';
    FDataModuleCreateCommand.Prepare(TServerMethods1_DataModuleCreate);
  end;
  if not Assigned(Sender) then
    FDataModuleCreateCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDSRestCommand(FDataModuleCreateCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FDataModuleCreateCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(Sender), True);
      if FInstanceOwner then
        Sender.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FDataModuleCreateCommand.Execute;
end;

function TServerMethods1Client.EchoString(Value: string; const ARequestFilter: string): string;
begin
  if FEchoStringCommand = nil then
  begin
    FEchoStringCommand := FConnection.CreateCommand;
    FEchoStringCommand.RequestType := 'GET';
    FEchoStringCommand.Text := 'TServerMethods1.EchoString';
    FEchoStringCommand.Prepare(TServerMethods1_EchoString);
  end;
  FEchoStringCommand.Parameters[0].Value.SetWideString(Value);
  FEchoStringCommand.Execute(ARequestFilter);
  Result := FEchoStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethods1Client.ReverseString(Value: string; const ARequestFilter: string): string;
begin
  if FReverseStringCommand = nil then
  begin
    FReverseStringCommand := FConnection.CreateCommand;
    FReverseStringCommand.RequestType := 'GET';
    FReverseStringCommand.Text := 'TServerMethods1.ReverseString';
    FReverseStringCommand.Prepare(TServerMethods1_ReverseString);
  end;
  FReverseStringCommand.Parameters[0].Value.SetWideString(Value);
  FReverseStringCommand.Execute(ARequestFilter);
  Result := FReverseStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethods1Client.CalcPreco(codprod: string; polpreco: Integer; peso: Currency; const ARequestFilter: string): Currency;
begin
  if FCalcPrecoCommand = nil then
  begin
    FCalcPrecoCommand := FConnection.CreateCommand;
    FCalcPrecoCommand.RequestType := 'GET';
    FCalcPrecoCommand.Text := 'TServerMethods1.CalcPreco';
    FCalcPrecoCommand.Prepare(TServerMethods1_CalcPreco);
  end;
  FCalcPrecoCommand.Parameters[0].Value.SetWideString(codprod);
  FCalcPrecoCommand.Parameters[1].Value.SetInt32(polpreco);
  FCalcPrecoCommand.Parameters[2].Value.AsCurrency := peso;
  FCalcPrecoCommand.Execute(ARequestFilter);
  Result := FCalcPrecoCommand.Parameters[3].Value.AsCurrency;
end;

function TServerMethods1Client.ConverterReais(Valor: Currency; const ARequestFilter: string): Currency;
begin
  if FConverterReaisCommand = nil then
  begin
    FConverterReaisCommand := FConnection.CreateCommand;
    FConverterReaisCommand.RequestType := 'GET';
    FConverterReaisCommand.Text := 'TServerMethods1.ConverterReais';
    FConverterReaisCommand.Prepare(TServerMethods1_ConverterReais);
  end;
  FConverterReaisCommand.Parameters[0].Value.AsCurrency := Valor;
  FConverterReaisCommand.Execute(ARequestFilter);
  Result := FConverterReaisCommand.Parameters[1].Value.AsCurrency;
end;

function TServerMethods1Client.ChecaProdutoExiste(Codprod: string; const ARequestFilter: string): Boolean;
begin
  if FChecaProdutoExisteCommand = nil then
  begin
    FChecaProdutoExisteCommand := FConnection.CreateCommand;
    FChecaProdutoExisteCommand.RequestType := 'GET';
    FChecaProdutoExisteCommand.Text := 'TServerMethods1.ChecaProdutoExiste';
    FChecaProdutoExisteCommand.Prepare(TServerMethods1_ChecaProdutoExiste);
  end;
  FChecaProdutoExisteCommand.Parameters[0].Value.SetWideString(Codprod);
  FChecaProdutoExisteCommand.Execute(ARequestFilter);
  Result := FChecaProdutoExisteCommand.Parameters[1].Value.GetBoolean;
end;

function TServerMethods1Client.DownloadFile(AFile: string; out Size: Int64; const ARequestFilter: string): TStream;
begin
  if FDownloadFileCommand = nil then
  begin
    FDownloadFileCommand := FConnection.CreateCommand;
    FDownloadFileCommand.RequestType := 'GET';
    FDownloadFileCommand.Text := 'TServerMethods1.DownloadFile';
    FDownloadFileCommand.Prepare(TServerMethods1_DownloadFile);
  end;
  FDownloadFileCommand.Parameters[0].Value.SetWideString(AFile);
  FDownloadFileCommand.Execute(ARequestFilter);
  Size := FDownloadFileCommand.Parameters[1].Value.GetInt64;
  Result := FDownloadFileCommand.Parameters[2].Value.GetStream(FInstanceOwner);
end;

function TServerMethods1Client.DownloadFile_Cache(AFile: string; out Size: Int64; const ARequestFilter: string): IDSRestCachedStream;
begin
  if FDownloadFileCommand_Cache = nil then
  begin
    FDownloadFileCommand_Cache := FConnection.CreateCommand;
    FDownloadFileCommand_Cache.RequestType := 'GET';
    FDownloadFileCommand_Cache.Text := 'TServerMethods1.DownloadFile';
    FDownloadFileCommand_Cache.Prepare(TServerMethods1_DownloadFile_Cache);
  end;
  FDownloadFileCommand_Cache.Parameters[0].Value.SetWideString(AFile);
  FDownloadFileCommand_Cache.ExecuteCache(ARequestFilter);
  Size := FDownloadFileCommand_Cache.Parameters[1].Value.GetInt64;
  Result := TDSRestCachedStream.Create(FDownloadFileCommand_Cache.Parameters[2].Value.GetString);
end;

function TServerMethods1Client.GetDadosProduto(CodProd: string; CodPolPreco: Integer; Aro: Integer; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetDadosProdutoCommand = nil then
  begin
    FGetDadosProdutoCommand := FConnection.CreateCommand;
    FGetDadosProdutoCommand.RequestType := 'GET';
    FGetDadosProdutoCommand.Text := 'TServerMethods1.GetDadosProduto';
    FGetDadosProdutoCommand.Prepare(TServerMethods1_GetDadosProduto);
  end;
  FGetDadosProdutoCommand.Parameters[0].Value.SetWideString(CodProd);
  FGetDadosProdutoCommand.Parameters[1].Value.SetInt32(CodPolPreco);
  FGetDadosProdutoCommand.Parameters[2].Value.SetInt32(Aro);
  FGetDadosProdutoCommand.Execute(ARequestFilter);
  if not FGetDadosProdutoCommand.Parameters[3].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetDadosProdutoCommand.Parameters[3].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetDadosProdutoCommand.Parameters[3].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetDadosProdutoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.GetDadosProduto_Cache(CodProd: string; CodPolPreco: Integer; Aro: Integer; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetDadosProdutoCommand_Cache = nil then
  begin
    FGetDadosProdutoCommand_Cache := FConnection.CreateCommand;
    FGetDadosProdutoCommand_Cache.RequestType := 'GET';
    FGetDadosProdutoCommand_Cache.Text := 'TServerMethods1.GetDadosProduto';
    FGetDadosProdutoCommand_Cache.Prepare(TServerMethods1_GetDadosProduto_Cache);
  end;
  FGetDadosProdutoCommand_Cache.Parameters[0].Value.SetWideString(CodProd);
  FGetDadosProdutoCommand_Cache.Parameters[1].Value.SetInt32(CodPolPreco);
  FGetDadosProdutoCommand_Cache.Parameters[2].Value.SetInt32(Aro);
  FGetDadosProdutoCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGetDadosProdutoCommand_Cache.Parameters[3].Value.GetString);
end;

function TServerMethods1Client.GetDadosProdutoLoja(CodProd: string; PolPreco: Integer; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetDadosProdutoLojaCommand = nil then
  begin
    FGetDadosProdutoLojaCommand := FConnection.CreateCommand;
    FGetDadosProdutoLojaCommand.RequestType := 'GET';
    FGetDadosProdutoLojaCommand.Text := 'TServerMethods1.GetDadosProdutoLoja';
    FGetDadosProdutoLojaCommand.Prepare(TServerMethods1_GetDadosProdutoLoja);
  end;
  FGetDadosProdutoLojaCommand.Parameters[0].Value.SetWideString(CodProd);
  FGetDadosProdutoLojaCommand.Parameters[1].Value.SetInt32(PolPreco);
  FGetDadosProdutoLojaCommand.Execute(ARequestFilter);
  if not FGetDadosProdutoLojaCommand.Parameters[2].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetDadosProdutoLojaCommand.Parameters[2].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetDadosProdutoLojaCommand.Parameters[2].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetDadosProdutoLojaCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.GetDadosProdutoLoja_Cache(CodProd: string; PolPreco: Integer; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetDadosProdutoLojaCommand_Cache = nil then
  begin
    FGetDadosProdutoLojaCommand_Cache := FConnection.CreateCommand;
    FGetDadosProdutoLojaCommand_Cache.RequestType := 'GET';
    FGetDadosProdutoLojaCommand_Cache.Text := 'TServerMethods1.GetDadosProdutoLoja';
    FGetDadosProdutoLojaCommand_Cache.Prepare(TServerMethods1_GetDadosProdutoLoja_Cache);
  end;
  FGetDadosProdutoLojaCommand_Cache.Parameters[0].Value.SetWideString(CodProd);
  FGetDadosProdutoLojaCommand_Cache.Parameters[1].Value.SetInt32(PolPreco);
  FGetDadosProdutoLojaCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGetDadosProdutoLojaCommand_Cache.Parameters[2].Value.GetString);
end;

function TServerMethods1Client.GetProdutos(Codprod: string; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetProdutosCommand = nil then
  begin
    FGetProdutosCommand := FConnection.CreateCommand;
    FGetProdutosCommand.RequestType := 'GET';
    FGetProdutosCommand.Text := 'TServerMethods1.GetProdutos';
    FGetProdutosCommand.Prepare(TServerMethods1_GetProdutos);
  end;
  FGetProdutosCommand.Parameters[0].Value.SetWideString(Codprod);
  FGetProdutosCommand.Execute(ARequestFilter);
  if not FGetProdutosCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetProdutosCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetProdutosCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetProdutosCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.GetProdutos_Cache(Codprod: string; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetProdutosCommand_Cache = nil then
  begin
    FGetProdutosCommand_Cache := FConnection.CreateCommand;
    FGetProdutosCommand_Cache.RequestType := 'GET';
    FGetProdutosCommand_Cache.Text := 'TServerMethods1.GetProdutos';
    FGetProdutosCommand_Cache.Prepare(TServerMethods1_GetProdutos_Cache);
  end;
  FGetProdutosCommand_Cache.Parameters[0].Value.SetWideString(Codprod);
  FGetProdutosCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGetProdutosCommand_Cache.Parameters[1].Value.GetString);
end;

function TServerMethods1Client.GetSaldo(CodProd: string; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetSaldoCommand = nil then
  begin
    FGetSaldoCommand := FConnection.CreateCommand;
    FGetSaldoCommand.RequestType := 'GET';
    FGetSaldoCommand.Text := 'TServerMethods1.GetSaldo';
    FGetSaldoCommand.Prepare(TServerMethods1_GetSaldo);
  end;
  FGetSaldoCommand.Parameters[0].Value.SetWideString(CodProd);
  FGetSaldoCommand.Execute(ARequestFilter);
  if not FGetSaldoCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetSaldoCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetSaldoCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetSaldoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.GetSaldo_Cache(CodProd: string; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetSaldoCommand_Cache = nil then
  begin
    FGetSaldoCommand_Cache := FConnection.CreateCommand;
    FGetSaldoCommand_Cache.RequestType := 'GET';
    FGetSaldoCommand_Cache.Text := 'TServerMethods1.GetSaldo';
    FGetSaldoCommand_Cache.Prepare(TServerMethods1_GetSaldo_Cache);
  end;
  FGetSaldoCommand_Cache.Parameters[0].Value.SetWideString(CodProd);
  FGetSaldoCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGetSaldoCommand_Cache.Parameters[1].Value.GetString);
end;

function TServerMethods1Client.ValidaUsuarioSenha(usuario: string; senha: string; const ARequestFilter: string): Boolean;
begin
  if FValidaUsuarioSenhaCommand = nil then
  begin
    FValidaUsuarioSenhaCommand := FConnection.CreateCommand;
    FValidaUsuarioSenhaCommand.RequestType := 'GET';
    FValidaUsuarioSenhaCommand.Text := 'TServerMethods1.ValidaUsuarioSenha';
    FValidaUsuarioSenhaCommand.Prepare(TServerMethods1_ValidaUsuarioSenha);
  end;
  FValidaUsuarioSenhaCommand.Parameters[0].Value.SetWideString(usuario);
  FValidaUsuarioSenhaCommand.Parameters[1].Value.SetWideString(senha);
  FValidaUsuarioSenhaCommand.Execute(ARequestFilter);
  Result := FValidaUsuarioSenhaCommand.Parameters[2].Value.GetBoolean;
end;

function TServerMethods1Client.GetConjunto(PolPreco: Integer; CodProd: string; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetConjuntoCommand = nil then
  begin
    FGetConjuntoCommand := FConnection.CreateCommand;
    FGetConjuntoCommand.RequestType := 'GET';
    FGetConjuntoCommand.Text := 'TServerMethods1.GetConjunto';
    FGetConjuntoCommand.Prepare(TServerMethods1_GetConjunto);
  end;
  FGetConjuntoCommand.Parameters[0].Value.SetInt32(PolPreco);
  FGetConjuntoCommand.Parameters[1].Value.SetWideString(CodProd);
  FGetConjuntoCommand.Execute(ARequestFilter);
  if not FGetConjuntoCommand.Parameters[2].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetConjuntoCommand.Parameters[2].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetConjuntoCommand.Parameters[2].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetConjuntoCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.GetConjunto_Cache(PolPreco: Integer; CodProd: string; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetConjuntoCommand_Cache = nil then
  begin
    FGetConjuntoCommand_Cache := FConnection.CreateCommand;
    FGetConjuntoCommand_Cache.RequestType := 'GET';
    FGetConjuntoCommand_Cache.Text := 'TServerMethods1.GetConjunto';
    FGetConjuntoCommand_Cache.Prepare(TServerMethods1_GetConjunto_Cache);
  end;
  FGetConjuntoCommand_Cache.Parameters[0].Value.SetInt32(PolPreco);
  FGetConjuntoCommand_Cache.Parameters[1].Value.SetWideString(CodProd);
  FGetConjuntoCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGetConjuntoCommand_Cache.Parameters[2].Value.GetString);
end;

function TServerMethods1Client.GetCampanhas(const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetCampanhasCommand = nil then
  begin
    FGetCampanhasCommand := FConnection.CreateCommand;
    FGetCampanhasCommand.RequestType := 'GET';
    FGetCampanhasCommand.Text := 'TServerMethods1.GetCampanhas';
    FGetCampanhasCommand.Prepare(TServerMethods1_GetCampanhas);
  end;
  FGetCampanhasCommand.Execute(ARequestFilter);
  if not FGetCampanhasCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetCampanhasCommand.Parameters[0].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetCampanhasCommand.Parameters[0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetCampanhasCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.GetCampanhas_Cache(const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetCampanhasCommand_Cache = nil then
  begin
    FGetCampanhasCommand_Cache := FConnection.CreateCommand;
    FGetCampanhasCommand_Cache.RequestType := 'GET';
    FGetCampanhasCommand_Cache.Text := 'TServerMethods1.GetCampanhas';
    FGetCampanhasCommand_Cache.Prepare(TServerMethods1_GetCampanhas_Cache);
  end;
  FGetCampanhasCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGetCampanhasCommand_Cache.Parameters[0].Value.GetString);
end;

function TServerMethods1Client.GetClientes(Local: string; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetClientesCommand = nil then
  begin
    FGetClientesCommand := FConnection.CreateCommand;
    FGetClientesCommand.RequestType := 'GET';
    FGetClientesCommand.Text := 'TServerMethods1.GetClientes';
    FGetClientesCommand.Prepare(TServerMethods1_GetClientes);
  end;
  FGetClientesCommand.Parameters[0].Value.SetWideString(Local);
  FGetClientesCommand.Execute(ARequestFilter);
  if not FGetClientesCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetClientesCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetClientesCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetClientesCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.GetClientes_Cache(Local: string; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetClientesCommand_Cache = nil then
  begin
    FGetClientesCommand_Cache := FConnection.CreateCommand;
    FGetClientesCommand_Cache.RequestType := 'GET';
    FGetClientesCommand_Cache.Text := 'TServerMethods1.GetClientes';
    FGetClientesCommand_Cache.Prepare(TServerMethods1_GetClientes_Cache);
  end;
  FGetClientesCommand_Cache.Parameters[0].Value.SetWideString(Local);
  FGetClientesCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGetClientesCommand_Cache.Parameters[1].Value.GetString);
end;

function TServerMethods1Client.GetRangePrecos(const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetRangePrecosCommand = nil then
  begin
    FGetRangePrecosCommand := FConnection.CreateCommand;
    FGetRangePrecosCommand.RequestType := 'GET';
    FGetRangePrecosCommand.Text := 'TServerMethods1.GetRangePrecos';
    FGetRangePrecosCommand.Prepare(TServerMethods1_GetRangePrecos);
  end;
  FGetRangePrecosCommand.Execute(ARequestFilter);
  if not FGetRangePrecosCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetRangePrecosCommand.Parameters[0].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetRangePrecosCommand.Parameters[0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetRangePrecosCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.GetRangePrecos_Cache(const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetRangePrecosCommand_Cache = nil then
  begin
    FGetRangePrecosCommand_Cache := FConnection.CreateCommand;
    FGetRangePrecosCommand_Cache.RequestType := 'GET';
    FGetRangePrecosCommand_Cache.Text := 'TServerMethods1.GetRangePrecos';
    FGetRangePrecosCommand_Cache.Prepare(TServerMethods1_GetRangePrecos_Cache);
  end;
  FGetRangePrecosCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGetRangePrecosCommand_Cache.Parameters[0].Value.GetString);
end;

function TServerMethods1Client.ChecaVendeporAro(Codprod: string; const ARequestFilter: string): Boolean;
begin
  if FChecaVendeporAroCommand = nil then
  begin
    FChecaVendeporAroCommand := FConnection.CreateCommand;
    FChecaVendeporAroCommand.RequestType := 'GET';
    FChecaVendeporAroCommand.Text := 'TServerMethods1.ChecaVendeporAro';
    FChecaVendeporAroCommand.Prepare(TServerMethods1_ChecaVendeporAro);
  end;
  FChecaVendeporAroCommand.Parameters[0].Value.SetWideString(Codprod);
  FChecaVendeporAroCommand.Execute(ARequestFilter);
  Result := FChecaVendeporAroCommand.Parameters[1].Value.GetBoolean;
end;

function TServerMethods1Client.ValidaUsuarioSenha2(id: string; senha: string; const ARequestFilter: string): Boolean;
begin
  if FValidaUsuarioSenha2Command = nil then
  begin
    FValidaUsuarioSenha2Command := FConnection.CreateCommand;
    FValidaUsuarioSenha2Command.RequestType := 'GET';
    FValidaUsuarioSenha2Command.Text := 'TServerMethods1.ValidaUsuarioSenha2';
    FValidaUsuarioSenha2Command.Prepare(TServerMethods1_ValidaUsuarioSenha2);
  end;
  FValidaUsuarioSenha2Command.Parameters[0].Value.SetWideString(id);
  FValidaUsuarioSenha2Command.Parameters[1].Value.SetWideString(senha);
  FValidaUsuarioSenha2Command.Execute(ARequestFilter);
  Result := FValidaUsuarioSenha2Command.Parameters[2].Value.GetBoolean;
end;

function TServerMethods1Client.GetDadosUsuario(id: string; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetDadosUsuarioCommand = nil then
  begin
    FGetDadosUsuarioCommand := FConnection.CreateCommand;
    FGetDadosUsuarioCommand.RequestType := 'GET';
    FGetDadosUsuarioCommand.Text := 'TServerMethods1.GetDadosUsuario';
    FGetDadosUsuarioCommand.Prepare(TServerMethods1_GetDadosUsuario);
  end;
  FGetDadosUsuarioCommand.Parameters[0].Value.SetWideString(id);
  FGetDadosUsuarioCommand.Execute(ARequestFilter);
  if not FGetDadosUsuarioCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetDadosUsuarioCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetDadosUsuarioCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetDadosUsuarioCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.GetDadosUsuario_Cache(id: string; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetDadosUsuarioCommand_Cache = nil then
  begin
    FGetDadosUsuarioCommand_Cache := FConnection.CreateCommand;
    FGetDadosUsuarioCommand_Cache.RequestType := 'GET';
    FGetDadosUsuarioCommand_Cache.Text := 'TServerMethods1.GetDadosUsuario';
    FGetDadosUsuarioCommand_Cache.Prepare(TServerMethods1_GetDadosUsuario_Cache);
  end;
  FGetDadosUsuarioCommand_Cache.Parameters[0].Value.SetWideString(id);
  FGetDadosUsuarioCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGetDadosUsuarioCommand_Cache.Parameters[1].Value.GetString);
end;

function TServerMethods1Client.GetTodosProdutos(const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetTodosProdutosCommand = nil then
  begin
    FGetTodosProdutosCommand := FConnection.CreateCommand;
    FGetTodosProdutosCommand.RequestType := 'GET';
    FGetTodosProdutosCommand.Text := 'TServerMethods1.GetTodosProdutos';
    FGetTodosProdutosCommand.Prepare(TServerMethods1_GetTodosProdutos);
  end;
  FGetTodosProdutosCommand.Execute(ARequestFilter);
  if not FGetTodosProdutosCommand.Parameters[0].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetTodosProdutosCommand.Parameters[0].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetTodosProdutosCommand.Parameters[0].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetTodosProdutosCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethods1Client.GetTodosProdutos_Cache(const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetTodosProdutosCommand_Cache = nil then
  begin
    FGetTodosProdutosCommand_Cache := FConnection.CreateCommand;
    FGetTodosProdutosCommand_Cache.RequestType := 'GET';
    FGetTodosProdutosCommand_Cache.Text := 'TServerMethods1.GetTodosProdutos';
    FGetTodosProdutosCommand_Cache.Prepare(TServerMethods1_GetTodosProdutos_Cache);
  end;
  FGetTodosProdutosCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGetTodosProdutosCommand_Cache.Parameters[0].Value.GetString);
end;

constructor TServerMethods1Client.Create(ARestConnection: TDSRestConnection);
begin
  inherited Create(ARestConnection);
end;

constructor TServerMethods1Client.Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ARestConnection, AInstanceOwner);
end;

destructor TServerMethods1Client.Destroy;
begin
  FDataModuleCreateCommand.DisposeOf;
  FEchoStringCommand.DisposeOf;
  FReverseStringCommand.DisposeOf;
  FCalcPrecoCommand.DisposeOf;
  FConverterReaisCommand.DisposeOf;
  FChecaProdutoExisteCommand.DisposeOf;
  FDownloadFileCommand.DisposeOf;
  FDownloadFileCommand_Cache.DisposeOf;
  FGetDadosProdutoCommand.DisposeOf;
  FGetDadosProdutoCommand_Cache.DisposeOf;
  FGetDadosProdutoLojaCommand.DisposeOf;
  FGetDadosProdutoLojaCommand_Cache.DisposeOf;
  FGetProdutosCommand.DisposeOf;
  FGetProdutosCommand_Cache.DisposeOf;
  FGetSaldoCommand.DisposeOf;
  FGetSaldoCommand_Cache.DisposeOf;
  FValidaUsuarioSenhaCommand.DisposeOf;
  FGetConjuntoCommand.DisposeOf;
  FGetConjuntoCommand_Cache.DisposeOf;
  FGetCampanhasCommand.DisposeOf;
  FGetCampanhasCommand_Cache.DisposeOf;
  FGetClientesCommand.DisposeOf;
  FGetClientesCommand_Cache.DisposeOf;
  FGetRangePrecosCommand.DisposeOf;
  FGetRangePrecosCommand_Cache.DisposeOf;
  FChecaVendeporAroCommand.DisposeOf;
  FValidaUsuarioSenha2Command.DisposeOf;
  FGetDadosUsuarioCommand.DisposeOf;
  FGetDadosUsuarioCommand_Cache.DisposeOf;
  FGetTodosProdutosCommand.DisposeOf;
  FGetTodosProdutosCommand_Cache.DisposeOf;
  inherited;
end;

end.


unit untConfiguracoes;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  untDefault, FMX.Objects, FMX.Controls.Presentation, FMX.Layouts, FMX.ExtCtrls, System.IOUtils,
  FMX.Gestures, FMX.DialogService.Async, FMX.DialogService, FMX.TMSCustomButton,
  FMX.TMSSpeedButton, System.Actions, FMX.ActnList, FMX.TMSBaseControl,
  FMX.TMSLabelEdit, FMX.Edit, System.Rtti, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.Components,
  Data.FireDACJSONReflect,

  // ***************************************

  Data.DB, IdFTP,
  {$IFDEF ANDROID}
  Androidapi.IOUtils,

//  Androidapi.Jni.GraphicsContentViewText,
//  Androidapi.Jni.JavaTypes,
//  Androidapi.Jni,
//  Androidapi.Jni.GraphicsContentViewText,
//  Androidapi.Jni.Net,
//  Androidapi.Jni.JavaTypes,
//  idUri,
//  Androidapi.Jni,
//  Androidapi.JNIBridge,
//  Androidapi.Helpers,
  Posix.Unistd,
  {$ENDIF}


  // ***************************************

  Data.Bind.DBScope, FMX.Effects, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdExplicitTLSClientServerBase;

type
  TfrmConfiguracoes = class(TfrmDefault)
    Label1: TLabel;
    Button1: TButton;
    VertScrollBox1: TVertScrollBox;
    Layout1: TLayout;
    Layout2: TLayout;
    Layout3: TLayout;
    Label3: TLabel;
    Line2: TLine;
    Layout4: TLayout;
    ActionList1: TActionList;
    btnAcessoAterar: TSpeedButton;
    btnAcessoSalvar: TSpeedButton;
    Label2: TLabel;
    Line1: TLine;
    Label4: TLabel;
    Label5: TLabel;
    btnSincronizar: TSpeedButton;
    actAcessoAlterar: TAction;
    btnAcessoCancelar: TSpeedButton;
    actAcessoSalvar: TAction;
    actAcessoCancelar: TAction;
    lblNomeRepres: TLabel;
    Layout5: TLayout;
    Label6: TLabel;
    Line3: TLine;
    SpeedButton1: TSpeedButton;
    actAtualizacaoAtualizar: TAction;
    edtAcessoUser: TEdit;
    edtAcessoSenha: TEdit;
    BindSourceDB1: TBindSourceDB;
    LinkControlToFieldID: TLinkControlToField;
    BindingsList1: TBindingsList;
    ckbAdmin: TCheckBox;
    LinkControlToField1: TLinkControlToField;
    LinkControlToField2: TLinkControlToField;
    LinkPropertyToFieldNOME: TLinkPropertyToField;
    RoundRect1: TRoundRect;
    imgRepres: TImage;
    LinkPropertyToFieldBitmap: TLinkPropertyToField;
    actSincronizar: TAction;
    lblAreas: TLabel;
    lblUltSincronizacao: TLabel;
    lblPosicaoProduto: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure btnVoltarClick(Sender: TObject);
    procedure actAcessoAlterarExecute(Sender: TObject);
    procedure actAcessoSalvarExecute(Sender: TObject);
    procedure actAcessoCancelarExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure actSincronizarExecute(Sender: TObject);
  private
    { Private declarations }
    procedure AcessoBotoes;
    procedure AtualizarApk;
    procedure AtualizaUsuario;
    procedure AtualizaProdutos;
  public
    { Public declarations }
  end;

var
  frmConfiguracoes: TfrmConfiguracoes;
  size            : Int64;

implementation

{$R *.fmx}

uses untLib, untMain, untDM, ClientModuleUnit1;

procedure TfrmConfiguracoes.actAcessoAlterarExecute(Sender: TObject);
begin
  inherited;

  DM.qrySystem.Edit;

  AcessoBotoes;

  if edtAcessoUser.CanFocus then
    edtAcessoUser.SetFocus;
end;



procedure TfrmConfiguracoes.actAcessoCancelarExecute(Sender: TObject);
begin
  inherited;
  DM.qrySystem.Cancel;
  AcessoBotoes;
end;

procedure TfrmConfiguracoes.actAcessoSalvarExecute(Sender: TObject);
begin
  inherited;
  TemInternet;

  if ClientModule1.ServerMethods1Client.ValidaUsuarioSenha2(edtAcessoUser.Text,edtAcessoSenha.Text) = False then
  begin
    ShowMessage('Erro de autentica��o!');
    Exit;
  end;


    try
//      DM.Trans.StartTransaction;


      DM.qrySystem.Post;
      DM.qrySystem.ApplyUpdates;

//      DM.Trans.Commit;


      AcessoBotoes;
      ShowMessage('Acesso Liberado!');
    Except on E:Exception do
      begin
        DM.qrySystem.Cancel;
//        DM.Trans.Rollback;
        AcessoBotoes;
        raise Exception.Create('Erro: ' + E.ToString);
      end;
    end;
end;

procedure TfrmConfiguracoes.actSincronizarExecute(Sender: TObject);
begin
  inherited;
  TemInternet;
  AtualizaUsuario;
  AtualizaProdutos;
  ShowMessage('Sincroniza��o finalizada');
end;

procedure TfrmConfiguracoes.AtualizaProdutos;
var
  LDataSetList : TFDJSONDataSets;
begin
  inherited;
    // Coletando dataset do servidor atraves do id do usuario (representante)
    DM.mt.Active := False;
    LDataSetList := ClientModule1.ServerMethods1Client.GetTodosProdutos;
    Assert(TFDJSONDataSetsReader.GetListCount(LDataSetList) = 1);
    DM.mt.AppendData(TFDJSONDataSetsReader.GetListValue(LDataSetList,0));

    DM.qryProdutos.Close;
    DM.qryProdutos.SQL.Clear;
    DM.qryProdutos.ExecSQL('DELETE FROM PRODUTOS');
    DM.conSQLite.CommitRetaining;

    DM.qryProdutos.Close;
    DM.qryProdutos.SQL.Clear;
    DM.qryProdutos.Open('SELECT * FROM PRODUTOS');

    DM.mt.First;
    while not DM.mt.Eof do
    begin
    lblPosicaoProduto.Text := DM.mt.FieldByName('CODIGO').AsString;
    Application.ProcessMessages;
    DM.qryAux.Close;
    DM.qryAux.SQL.Clear;
    DM.qryAux.SQL.Add('INSERT INTO PRODUTOS(   [ID], '+
                                              '[CODIGO], '+
                                              '[CODIGO_SEM_PONTOS], '+
                                              '[VLR_AU_ICMS_18], '+
                                              '[VLR_AU_ICMS_12], '+
                                              '[VLR_AU_ICMS_7], '+
                                              '[VLR_AU_EXP], '+
                                              '[VLR_AU_ICMS_MANAUS], '+
                                              '[COTACAO_AU], '+
                                              '[COTACAO_US], '+
                                              '[DESCRICAO_PRODUTO], '+
                                              '[ATIVO], '+
                                              '[GRUPO], '+
                                              '[SUBGRUPO], '+
                                              '[TEOR], '+
                                              '[UNIDADE], '+
                                              '[DESCRICAO_COLECAO], '+
                                              '[DESCRICAO_LINHA], '+
                                              '[CONTROLADO_ARO], '+
                                              '[DTH_INATIVO], '+
                                              '[DTH_CRIACAO], '+
                                              '[PESO_MEDIO], '+
                                              '[QTD_BRILHANTES], '+
                                              '[QUIL_BRILHANTES], '+
                                              '[QTD_PEDRAS], '+
                                              '[QUIL_PEDRAS], '+
                                              '[ALTURA], '+
                                              '[LARGURA], '+
                                              '[DIAMETRO], '+
                                              '[PERFIL_INTERNO], '+
                                              '[PERFIL_EXTERNO], '+
                                              '[DESCRICAO_PEDRAS], '+
                                              '[ARO]) '+
                                   'VALUES(    :ID, '+
                                              ':CODIGO, '+
                                              ':CODIGO_SEM_PONTOS, '+
                                              ':VLR_AU_ICMS_18, '+
                                              ':VLR_AU_ICMS_12, '+
                                              ':VLR_AU_ICMS_7, '+
                                              ':VLR_AU_EXP, '+
                                              ':VLR_AU_ICMS_MANAUS, '+
                                              ':COTACAO_AU, '+
                                              ':COTACAO_US, '+
                                              ':DESCRICAO_PRODUTO, '+
                                              ':ATIVO, '+
                                              ':GRUPO, '+
                                              ':SUBGRUPO, '+
                                              ':TEOR, '+
                                              ':UNIDADE, '+
                                              ':DESCRICAO_COLECAO, '+
                                              ':DESCRICAO_LINHA, '+
                                              ':CONTROLADO_ARO, '+
                                              ':DTH_INATIVO, '+
                                              ':DTH_CRIACAO, '+
                                              ':PESO_MEDIO, '+
                                              ':QTD_BRILHANTES, '+
                                              ':QUIL_BRILHANTES, '+
                                              ':QTD_PEDRAS, '+
                                              ':QUIL_PEDRAS, '+
                                              ':ALTURA, '+
                                              ':LARGURA, '+
                                              ':DIAMETRO, '+
                                              ':PERFIL_INTERNO, '+
                                              ':PERFIL_EXTERNO, '+
                                              ':DESCRICAO_PEDRAS, '+
                                              ':ARO) ');
      DM.qryAux.Params[0].AsString   := DM.mt.FieldByName('ID').AsString;
      DM.qryAux.Params[1].AsString   := DM.mt.FieldByName('CODIGO').AsString;
      DM.qryAux.Params[2].AsString   := DM.mt.FieldByName('CODIGO_SEM_PONTOS').AsString;
      DM.qryAux.Params[3].AsFloat    := DM.mt.FieldByName('VLR_AU_ICMS_18').AsFloat;
      DM.qryAux.Params[4].AsFloat    := DM.mt.FieldByName('VLR_AU_ICMS_12').AsFloat;
      DM.qryAux.Params[5].AsFloat    := DM.mt.FieldByName('VLR_AU_ICMS_7').AsFloat;
      DM.qryAux.Params[6].AsFloat    := DM.mt.FieldByName('VLR_AU_EXP').AsFloat;
      DM.qryAux.Params[7].AsFloat    := DM.mt.FieldByName('VLR_AU_ICMS_MANAUS').AsFloat;
      DM.qryAux.Params[8].AsFloat    := DM.mt.FieldByName('COTACAO_AU').AsFloat;
      DM.qryAux.Params[9].AsFloat    := DM.mt.FieldByName('COTACAO_US').AsFloat;
      DM.qryAux.Params[10].AsString  := DM.mt.FieldByName('DESCRICAO_PRODUTO').AsString;
      DM.qryAux.Params[11].AsString  := DM.mt.FieldByName('ATIVO').AsString;
      DM.qryAux.Params[12].AsString  := DM.mt.FieldByName('GRUPO').AsString;
      DM.qryAux.Params[13].AsString  := DM.mt.FieldByName('SUBGRUPO').AsString;
      DM.qryAux.Params[14].AsString  := DM.mt.FieldByName('TEOR').AsString;
      DM.qryAux.Params[15].AsString  := DM.mt.FieldByName('UNIDADE').AsString;
      DM.qryAux.Params[16].AsString  := DM.mt.FieldByName('DESCRICAO_COLECAO').AsString;
      DM.qryAux.Params[17].AsString  := DM.mt.FieldByName('DESCRICAO_LINHA').AsString;
      DM.qryAux.Params[18].AsString  := DM.mt.FieldByName('CONTROLADO_ARO').AsString;
      DM.qryAux.Params[19].AsDate    := DM.mt.FieldByName('DTH_INATIVO').AsDateTime;
      DM.qryAux.Params[20].AsDate    := DM.mt.FieldByName('DTH_CRIACAO').AsDateTime;
      DM.qryAux.Params[21].AsFloat   := DM.mt.FieldByName('PESO_MEDIO').AsFloat;
      DM.qryAux.Params[22].AsFloat   := DM.mt.FieldByName('QTD_BRILHANTES').AsFloat;
      DM.qryAux.Params[23].AsFloat   := DM.mt.FieldByName('QUIL_BRILHANTES').AsFloat;
      DM.qryAux.Params[24].AsFloat   := DM.mt.FieldByName('QTD_PEDRAS').AsFloat;
      DM.qryAux.Params[25].AsFloat   := DM.mt.FieldByName('QUIL_PEDRAS').AsFloat;
      DM.qryAux.Params[26].AsFloat   := DM.mt.FieldByName('ALTURA').AsFloat;
      DM.qryAux.Params[27].AsFloat   := DM.mt.FieldByName('LARGURA').AsFloat;
      DM.qryAux.Params[28].AsFloat   := DM.mt.FieldByName('DIAMETRO').AsFloat;
      DM.qryAux.Params[29].AsString  := DM.mt.FieldByName('PERFIL_INTERNO').AsString;
      DM.qryAux.Params[30].AsString  := DM.mt.FieldByName('PERFIL_EXTERNO').AsString;
      DM.qryAux.Params[31].AsString  := DM.mt.FieldByName('DESCRICAO_PEDRAS').AsString;
      DM.qryAux.Params[32].AsInteger := DM.mt.FieldByName('ARO').AsInteger;
      DM.qryAux.ExecSQL;




    {  DM.qryProdutos.Insert;

      DM.qryProdutosID.AsString   := DM.mt.FieldByName('ID').AsString;
      DM.qryProdutosCODIGO.AsString   := DM.mt.FieldByName('CODIGO').AsString;
      DM.qryProdutosCODIGO_SEM_PONTOS.AsString   := DM.mt.FieldByName('CODIGO_SEM_PONTOS').AsString;
      DM.qryProdutosVLR_AU_ICMS_18.AsFloat := StrToFloat( FormatFloat( '#,##0.00' , DM.mt.FieldByName('VLR_AU_ICMS_18').AsFloat));
      DM.qryProdutosVLR_AU_ICMS_12.AsFloat    := DM.mt.FieldByName('VLR_AU_ICMS_12').AsFloat;
      DM.qryProdutosVLR_AU_ICMS_7.AsFloat    := DM.mt.FieldByName('VLR_AU_ICMS_7').AsFloat;
      DM.qryProdutosVLR_AU_EXP.AsFloat    := DM.mt.FieldByName('VLR_AU_EXP').AsFloat;
      DM.qryProdutosVLR_AU_ICMS_MANAUS.AsFloat    := DM.mt.FieldByName('VLR_AU_ICMS_MANAUS').AsFloat;
      DM.qryProdutosCOTACAO_AU.AsFloat    := DM.mt.FieldByName('COTACAO_AU').AsFloat;
      DM.qryProdutosCOTACAO_US.AsFloat    := DM.mt.FieldByName('COTACAO_US').AsFloat;
      DM.qryProdutosDESCRICAO_PRODUTO.AsString  := DM.mt.FieldByName('DESCRICAO_PRODUTO').AsString;
      DM.qryProdutosATIVO.AsString  := DM.mt.FieldByName('ATIVO').AsString;
      DM.qryProdutosGRUPO.AsString  := DM.mt.FieldByName('GRUPO').AsString;
      DM.qryProdutosSUBGRUPO.AsString  := DM.mt.FieldByName('SUBGRUPO').AsString;
      DM.qryProdutosTEOR.AsInteger  := DM.mt.FieldByName('TEOR').AsInteger;
      DM.qryProdutosUNIDADE.AsString  := DM.mt.FieldByName('UNIDADE').AsString;
      DM.qryProdutosDESCRICAO_COLECAO.AsString  := DM.mt.FieldByName('DESCRICAO_COLECAO').AsString;
      DM.qryProdutosDESCRICAO_LINHA.AsString  := DM.mt.FieldByName('DESCRICAO_LINHA').AsString;
      DM.qryProdutosCONTROLADO_ARO.AsString  := DM.mt.FieldByName('CONTROLADO_ARO').AsString;
      DM.qryProdutosDTH_INATIVO.AsDateTime    := DM.mt.FieldByName('DTH_INATIVO').AsDateTime;
      DM.qryProdutosDTH_CRIACAO.AsDateTime    := DM.mt.FieldByName('DTH_CRIACAO').AsDateTime;
      DM.qryProdutosPESO_MEDIO.AsFloat   := DM.mt.FieldByName('PESO_MEDIO').AsFloat;
      DM.qryProdutosQTD_BRILHANTES.AsFloat   := DM.mt.FieldByName('QTD_BRILHANTES').AsFloat;
      DM.qryProdutosQUIL_BRILHANTES.AsFloat   := DM.mt.FieldByName('QUIL_BRILHANTES').AsFloat;
      DM.qryProdutosQTD_PEDRAS.AsFloat   := DM.mt.FieldByName('QTD_PEDRAS').AsFloat;
      DM.qryProdutosQUIL_PEDRAS.AsFloat   := DM.mt.FieldByName('QUIL_PEDRAS').AsFloat;
      DM.qryProdutosALTURA.AsFloat   := DM.mt.FieldByName('ALTURA').AsFloat;
      DM.qryProdutosLARGURA.AsFloat   := DM.mt.FieldByName('LARGURA').AsFloat;
      DM.qryProdutosDIAMETRO.AsFloat   := DM.mt.FieldByName('DIAMETRO').AsFloat;
      DM.qryProdutosPERFIL_INTERNO.AsString  := DM.mt.FieldByName('PERFIL_INTERNO').AsString;
      DM.qryProdutosPERFIL_EXTERNO.AsString  := DM.mt.FieldByName('PERFIL_EXTERNO').AsString;
      DM.qryProdutosDESCRICAO_PEDRAS.AsString  := DM.mt.FieldByName('DESCRICAO_PEDRAS').AsString;
      DM.qryProdutosARO.AsInteger := DM.mt.FieldByName('ARO').AsInteger;

      DM.qryProdutos.Post;
      DM.qryProdutos.ApplyUpdates;     }
      DM.mt.Next;
    end;

    dm.conSQLite.CommitRetaining;
end;

procedure TfrmConfiguracoes.AtualizarApk;
begin

end;

procedure TfrmConfiguracoes.AtualizaUsuario;
var
  LDataSetList : TFDJSONDataSets;
  vMemStrImg : TMemoryStream;
begin
  inherited;
    // Coletando dataset do servidor atraves do id do usuario (representante)
    DM.mt.Active := False;
    LDataSetList := ClientModule1.ServerMethods1Client.GetDadosUsuario(DM.qrySystemID.AsString);
    // Update ListView
    Assert(TFDJSONDataSetsReader.GetListCount(LDataSetList) = 1);
    DM.mt.AppendData(TFDJSONDataSetsReader.GetListValue(LDataSetList,0));

    if DM.mt.FieldByName('id').AsString = '' then
    begin
      ShowMessage('Sincroniza��o do usu�rio n�o retornou dados. Verifique se as credenciais est�o corretas.');
      exit;
    end;
   // frmMain.conPrecos.StartTransaction;
  {
   with DM.qryAux do
   begin
     Close;
     Sql.Clear;
     SQL.Add('UPDATE SYSTEM SET DTH_ULTIMA_ATUALIZACAO = :DTH_ULTIMA_ATUALIZACAO ' +
                               'IMAGEM = :IMAGEM '+
                               'NOME = :NOME ' +
                               'VERSAO = :VERSAO '+
                               'ADMIN = :ADMIN '+
                               'AREAS = :AREAS ');
     ParamByName('DTH_ULTIMA_ATUALIZACAO').AsDateTime := Now;
     ParamByName('IMAGEM').IsNull;


   end;
   }




  // vMemStrImg := TMemoryStream.Create;
  // DM.mt.FieldByName('IMG').DataType := ftBlob;
  // vMemStrImg.LoadFromStream(DM.mt.FieldByName('IMG').DataType.ftBlob);

 //     qrySystemIMAGEM.SaveToStream(vMemStrImg);

   DM.qrySystem.Edit;
   DM.qrySystemDTH_ULTIMA_ATUALIZACAO.AsDateTime := now;

 // DM.qrySystemIMAGEM.DataType.ftBlob := DM.mt.FieldByName('IMG').DataType.ftBlob;  // LoadFromStream(vMemStrImg);
   DM.qrySystemNOME.AsString := DM.mt.FieldByName('NOME').AsString;
  // DM.qrySystemVERSAO.AsString := DM.mt.FieldByName('VERSAO').AsString;
   if DM.mt.FieldByName('ADMIN').AsString = 'S' then
     DM.qrySystemADMIN.AsBoolean := True
   else
     DM.qrySystemADMIN.AsBoolean := false;
   DM.qrySystemAREAS.AsString := DM.mt.FieldByName('AREAS').AsString;
   lblAreas.Text := '�rea(s) Autorizada(s): ' +DM.mt.FieldByName('AREAS').AsString;
   lblUltSincronizacao.Text := '�ltima sincroniza��o: ' +DateTimeToStr(DM.qrySystemDTH_ULTIMA_ATUALIZACAO.AsDateTime);
   DM.qrySystem.Post;
   DM.qrySystem.ApplyUpdates;


   FreeAndNil(vMemStrImg);


end;

procedure TfrmConfiguracoes.AcessoBotoes;
begin
  btnAcessoAterar.Visible   := (DM.qrySystem.State = dsBrowse);
  btnAcessoCancelar.Visible := (DM.qrySystem.State in dsEditModes);
  btnAcessoSalvar.Visible   := (DM.qrySystem.State in dsEditModes);
  edtAcessoUser.Enabled  := (DM.qrySystem.State in dsEditModes);
  edtAcessoSenha.Enabled := (DM.qrySystem.State in dsEditModes);
end;

procedure TfrmConfiguracoes.btnVoltarClick(Sender: TObject);
begin
  inherited;
  frmConfiguracoes.Hide;
end;

procedure TfrmConfiguracoes.Button1Click(Sender: TObject);
begin
  inherited;
  TDialogServiceAsync.MessageDialog('Deseja fechar o aplicativo',
                                     TMsgDlgType.mtConfirmation,
                                    [TMsgDlgBtn.mbYes,TMsgDlgBtn.mbNo],
                                    TMsgDlgBtn.mbNo,
                                     0,
                                     procedure(const AResult:TModalResult)
                                     begin
                                       if AResult = mrYes then
                                       begin
                                         FMX.DialogService.TDialogService.ShowMessage('sim');
                                       end
                                       else
                                         FMX.DialogService.TDialogService.ShowMessage('n�o');
                                     end
                                     );

end;

procedure TfrmConfiguracoes.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  inherited;
  if key = vkHardwareBack then
  begin
    Key := 0;

    if DM.qrySystem.State in [dsEdit, dsInsert] then
    begin
      ShowMessage('Termine as altera��es de acesso clicando em salvar ou cancelar');
    end
    else
      frmConfiguracoes.Hide;
  end;
end;

procedure TfrmConfiguracoes.FormShow(Sender: TObject);
begin
  inherited;
  AcessoBotoes;
  lblAreas.Text := '�rea(s) Autorizada(s): ' +DM.qrySystemAREAS.AsString;
  lblUltSincronizacao.Text := '�ltima sincroniza��o: ' +DateTimeToStr(DM.qrySystemDTH_ULTIMA_ATUALIZACAO.AsDateTime);
end;

end.

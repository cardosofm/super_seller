unit untMain;

interface

uses
  System.SysUtils,

  {$IF DEFINED(IOS) OR DEFINED(ANDROID)}
   FMX.Helpers.Android, FMX.Platform.Android,
  {$ENDIF}

  System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.Effects, FMX.MultiView,
  FMX.Layouts, System.ImageList, FMX.ImgList, System.Actions, FMX.ActnList,
  FMX.Platform, FMX.VirtualKeyboard, FMX.DialogService.Async,
  FMX.DialogService, System.Rtti, FMX.Grid.Style,
  Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.Grid, FMX.ScrollBox,
  FMX.Grid, Data.Bind.DBScope, FMX.TMSLiveGridDataBinding, FMX.TMSBaseControl,
  FMX.TMSGridCell, FMX.TMSGridOptions, FMX.TMSGridData, FMX.TMSCustomGrid,
  FMX.TMSLiveGrid, Data.Bind.Controls, Fmx.Bind.Navigator, FMX.TMSListView,
  FireDAC.Stan.StorageBin;

type
  TfrmMain = class(TForm)
    Rectangle1: TRectangle;
    imgLogo: TImage;
    btnMenu: TSpeedButton;
    ImageList1: TImageList;
    MultiView1: TMultiView;
    Layout1: TLayout;
    Layout2: TLayout;
    Layout3: TLayout;
    lblUser: TLabel;
    rctTop: TRectangle;
    Layout4: TLayout;
    Circle2: TCircle;
    Image2: TImage;
    actMain: TActionList;
    actConfiguracoes: TAction;
    Label2: TLabel;
    Layout5: TLayout;
    Image3: TImage;
    Label3: TLabel;
    Layout6: TLayout;
    Image4: TImage;
    Label4: TLabel;
    Layout7: TLayout;
    Image1: TImage;
    Label5: TLabel;
    Layout8: TLayout;
    Image5: TImage;
    Label6: TLabel;
    lytTop: TLayout;
    Layout9: TLayout;
    Image6: TImage;
    Label7: TLabel;
    Image7: TImage;
    lblVersao: TLabel;
    Layout10: TLayout;
    Image8: TImage;
    Label1: TLabel;
    imgRepres: TImage;
    BindingsList1: TBindingsList;
    LinkPropertyToFieldIMAGEM: TLinkPropertyToField;
    FDStanStorageBinLink1: TFDStanStorageBinLink;
    actConsultaPreco: TAction;
    Button1: TButton;
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure actConfiguracoesExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure actConsultaPrecoExecute(Sender: TObject);

  private
    { Private declarations }
    procedure CarregaFotoMain;
  public

  end;

var
  frmMain: TfrmMain;

implementation

{$R *.fmx}

uses untConfiguracoes, untLib, untDM, untTestes, untConsProdutos;

procedure TfrmMain.actConfiguracoesExecute(Sender: TObject);
begin
  frmConfiguracoes.Show;
end;

procedure TfrmMain.actConsultaPrecoExecute(Sender: TObject);
begin
  frmConsProdutos.Show
end;

procedure TfrmMain.CarregaFotoMain;
//var
//  Foto : TStream;
begin
//  Foto := TMemoryStream.Create;
//  DM.qrySystemIMAGEM.SaveToStream(Foto);
//  crcImg.Fill.Bitmap.Bitmap.LoadFromStream(Foto);
//  FreeAndNil(Foto);
end;

procedure TfrmMain.FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
  Shift: TShiftState);
begin
  if key = vkHardwareBack then
  begin
    Key := 0;

    TDialogServiceAsync.MessageDialog('Deseja fechar o aplicativo',
                                       TMsgDlgType.mtConfirmation,
                                      [TMsgDlgBtn.mbYes,TMsgDlgBtn.mbNo],
                                       TMsgDlgBtn.mbNo,
                                       0,
                                       procedure(const AResult:TModalResult)
                                       begin
                                         if AResult = mrYes then
                                         begin
                                           Application.Terminate;
                                         end
                                       end
                                       );


  end;
end;



procedure TfrmMain.FormShow(Sender: TObject);

begin
  lblUser.Text   := ExecSQL('SELECT NOME FROM SYSTEM');
  lblVersao.Text := 'Vers�o: ' + ExecSQL('SELECT VERSAO FROM SYSTEM');
//  CarregaFotoMain;
end;

end.



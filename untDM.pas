unit untDM;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, FireDAC.FMXUI.Wait, Data.DB,
  FireDAC.Comp.Client, System.IOUtils, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet;

type
  TDM = class(TDataModule)
    conSQLite: TFDConnection;
    qrySystem: TFDQuery;
    mt: TFDMemTable;
    qrySystemID: TStringField;
    qrySystemSENHA: TStringField;
    qrySystemDTH_ULTIMA_ATUALIZACAO: TSQLTimeStampField;
    qrySystemIMAGEM: TBlobField;
    qrySystemNOME: TStringField;
    qrySystemVERSAO: TStringField;
    qrySystemADMIN: TBooleanField;
    qrySystemAREAS: TStringField;
    qryAux: TFDQuery;
    qryProdutos: TFDQuery;
    qryProdutosID: TStringField;
    qryProdutosCODIGO: TStringField;
    qryProdutosCODIGO_SEM_PONTOS: TStringField;
    qryProdutosVLR_AU_ICMS_18: TBCDField;
    qryProdutosVLR_AU_ICMS_12: TBCDField;
    qryProdutosVLR_AU_ICMS_7: TBCDField;
    qryProdutosVLR_AU_EXP: TBCDField;
    qryProdutosVLR_AU_ICMS_MANAUS: TBCDField;
    qryProdutosCOTACAO_AU: TBCDField;
    qryProdutosCOTACAO_US: TBCDField;
    qryProdutosDESCRICAO_PRODUTO: TStringField;
    qryProdutosATIVO: TStringField;
    qryProdutosGRUPO: TStringField;
    qryProdutosSUBGRUPO: TStringField;
    qryProdutosTEOR: TIntegerField;
    qryProdutosUNIDADE: TStringField;
    qryProdutosDESCRICAO_COLECAO: TStringField;
    qryProdutosDESCRICAO_LINHA: TStringField;
    qryProdutosCONTROLADO_ARO: TStringField;
    qryProdutosDTH_INATIVO: TSQLTimeStampField;
    qryProdutosDTH_CRIACAO: TSQLTimeStampField;
    qryProdutosPESO_MEDIO: TBCDField;
    qryProdutosQTD_BRILHANTES: TBCDField;
    qryProdutosQUIL_BRILHANTES: TBCDField;
    qryProdutosQTD_PEDRAS: TBCDField;
    qryProdutosQUIL_PEDRAS: TBCDField;
    qryProdutosALTURA: TBCDField;
    qryProdutosLARGURA: TBCDField;
    qryProdutosDIAMETRO: TBCDField;
    qryProdutosPERFIL_INTERNO: TStringField;
    qryProdutosPERFIL_EXTERNO: TStringField;
    qryProdutosDESCRICAO_PEDRAS: TStringField;
    qryProdutosARO: TIntegerField;
    FDUpdateSQL1: TFDUpdateSQL;
    Command: TFDCommand;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TDM.DataModuleCreate(Sender: TObject);
var
  sPath: String;
  //qry : TFDQuery;
begin
  with conSQLite do
  begin
//    Params.Values['DriverID'] := 'SQLite';
//    Params.Values['OpenMode'] := 'ReadWrite';
    {$IF DEFINED(IOS) OR DEFINED(ANDROID)}
      try
        sPath := TPath.Combine(TPath.GetDocumentsPath, 'SSDB.db');
        Params.Values['Database'] := TPath.Combine(TPath.GetDocumentsPath, 'SSDB.db');
        Connected := True;
      except on E: Exception do
        begin
          raise Exception.Create('Erro: ' +E.ToString);
        end;
      end;
    {$ELSE}
      try
        Params.Values['Database'] := 'D:\PROGRAMACAO\PROJETOS\super_seller\database\SSDB.db';
        Connected := True;
      except on E: Exception do
        begin
          raise Exception.Create('Erro: ' +E.ToString);
        end;
      end;
    {$ENDIF}
  end;

  // abre a query para poder testar se existe usuario
//  qrySystem.Active := False;
    qrySystem.Active := True;
//  qrySystem.Open;
//  DM.qryClientes.Active := True;

end;

end.

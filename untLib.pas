unit untLib;

interface

uses
  FMX.DialogService, FMX.Dialogs, System.UITypes, FMX.Forms, System.Classes,


  System.SysUtils, {System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, FireDAC.FMXUI.Wait, Data.DB,
  FireDAC.Comp.Client, System.IOUtils, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet,   }
  FireDAC.Comp.Client;

//  Androidapi.JNI.Network;


function DlgBox(AMessage: String): String;
//function GetTableField(Field, ): String;
function ExecSQL(SQL:String): String;
function TemInternet: Boolean;


implementation

uses untDM;

function DlgBox(AMessage: String): String;
var
  lResultStr: String;
begin
  lResultStr:='';
  TDialogService.PreferredMode:=TDialogService.TPreferredMode.Platform;
  TDialogService.MessageDialog(AMessage, TMsgDlgType.mtConfirmation,
    FMX.Dialogs.mbYesNo, TMsgDlgBtn.mbYes, 0,
    procedure(const AResult: TModalResult)
    begin
      case AResult of
        mrYes: lResultStr:='S';
        mrNo:  lResultStr:='N';
      end;
    end);
  Result:='S';
end;

function ExecSQL(SQL:String) : String;
var
  Qry : TFDQuery;
  teste :string;
begin
  Qry := TFDQuery.Create(Nil);
  try
    Qry.Connection := DM.conSQLite;
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add(SQL);
    Qry.Open;
    teste := Qry.Fields[0].AsString;
    Result := Qry.Fields[0].AsString;
  finally
    FreeAndNil(Qry);
  end;
end;

function TemInternet: Boolean;
begin
 {  try
     if not IsConnected then
       raise Exception.Create('Dispositivo sem conex�o com internet');
   except on E:Exception do
     raise Exception.Create('Erro: ' +e.ToString);
   end;  }
end;



end.
